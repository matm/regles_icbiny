Règles pour ICBINY: I Can't Believe It's Not Yahtzee
====================================================

Matériel:
---------

6 dés à 6 faces


Objectif:
---------

Atteindre 10 000 points


Règles:
-------

* Au début de son tour de jeu, le joueur a 6 dés disponibles
* Après avoir lancé les dés disponibles, le joueur peut collecter de 1 à 3 dés. Ces dés ne sont plus disponibles.
* Il choisit de relancer les dés disponibles ou d'arrêter son tour.
* S'il arrête son tour, on calcule les points associés aux dés collectés, qu'on ajoute au score du joueur, et la main passe.
* S'il relance et qu'il n'y a pas de dés à collecter, le tour de jeu est perdu, le score du joueur reste le même, et la main passe.
* Si les 6 dés ont été collectés, les 6 dés deviennent disponibles. Le joueur peut donc choisir de relancer ou d'arrêter.
* Si le lancer de dés donne 4 x `2`, le tour de jeu est perdu, et la main passe.

### Collecte:

  * Chaque `1` rapporte 100 
  * Chaque `5` rapporte 500 
  * Un brelan de `1` rapporte 1000
  * Les autres brelans rapportent la valeur du dé x100  
  * Trois paires rapportent 1500
  * Une suite de 6 dés rapporte 1500
  * Si vous lancez le 6e dé seul:
    *  Vous faites `1`, vous rapportez 500
    * Vous ne faites ni `1` ni `5`, vous pouvez relancer *une* fois


